let text = {
    content: "12,23,34,45,56,67,78,89",

    [Symbol.iterator]: function(){
        return {
            data: this.content.split(','),
            index: 0,

            next(){
                if (this.index < this.data.length){
                    return {done: false, value: Number(this.data[this.index++])}
                }
                else {
                    return {done: true}
                }
            }


        }
    }
}

for (let n of text){
    console.log(n);
}