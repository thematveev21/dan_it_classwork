

// function declaration

// function sayHelloTo(firstname){
//     alert(`Hello to ${firstname}`);
// }



// function expression

// let sayHelloTo = function(firstname){
//     alert(`Hello to ${firstname}`);
// }


// function day(){
//     alert("Day now!");
// }

// function night(){
//     alert("Night now!");
// }

// function whatTime(time, ifDay, ifNight){
//     if (time > 12){
//         ifDay();
//     }
//     else {
//         ifNight();
//     }
// }

// whatTime(4, day, night);

// whatTime(
//     4,
//     function(){alert("day now")},
//     function(){alert("night now")}
// );


function requestData(whenDone){
    console.log("Requesting data from server...");
    console.log("Data received!")
    whenDone();
}



// requestData(
//     function(){console.log("When done!")}
// )

// requestData(
//     () => console.log("When done callback!")
// )


// let summa = function(a, b){
//     return a + b;
// }

// let summa = (a, b) => a + b;

// console.log(summa(12, 23))


function summa(a, b, c){
    return a + b + c;
}


