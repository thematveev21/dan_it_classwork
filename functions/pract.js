// EX 1 ------------

// function promptNumber(text="Enter something", defaultValue=0){
//     let num = prompt(text, defaultValue);
//     let res = Number(num);
//     return res;
// }

// function countPerimetr(a, b){
//     let P = (a + b) * 2;
//     return P;
// }

// let a = promptNumber("Enter number 1", 10);
// let b = promptNumber();

// let result = countPerimetr(a, b);

// console.log(result);

// EX 2

function askNumber(){
    let n = prompt("Enter number:");
    let converted = Number(n);
    if (isNaN(converted) || n === ''){
        return false;
    }
    else {
        return converted;
    }
}

function askNumberTillCorrect(){
    let number;
    do {
        number = askNumber();
    }while(!number && number !== 0)

    return number;
}



let a = askNumberTillCorrect();
let b = askNumberTillCorrect();

console.log(a + b);



// while (true){
//     let n = askNumber();
//     if (n){
//         console.log(n);
//         break;
//     }
// }