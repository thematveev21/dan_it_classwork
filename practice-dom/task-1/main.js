// enter data
function getDataFromUser(text){

    while (true){

        let size = prompt(text);
        let n = Number(size);

        if (!isNaN(n) && n > 0) {
            return n;
        }
    }

}

// create element

function createSquare(h, w){
    // creating div block
    let block = document.createElement('div');
    block.style.width = String(w) + 'px';
    block.style.height = String(h) + 'px';
    block.style.backgroundColor = 'red';
    block.classList.add('square')

    return block;
}


// insert element

function insertIntoBody(element){
    document.body.prepend(element);
}


// main code

let width = getDataFromUser("Enter width:");
let height = getDataFromUser("Enter height:");
console.log(width, height);
let rect = createSquare(height, width);
console.log(rect);
insertIntoBody(rect);