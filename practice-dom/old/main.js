// let a = new Date(2023, 3, 8, 12, 5, 34, 567);
// console.log(a);
// let b = new Date();
// console.log(b)

// let today = new Date(2023, 3, 8);
// console.log(today);


// let now = new Date();

// let dateString = `${now.getDate()}/${now.getMonth() + 1}/${now.getFullYear()}`;
// console.log(dateString);


// console.log(now.toTimeString())

// let start = new Date();

// for (let i = 0; i < 10000000; i++){
//     n = i ** i
// }

// let end = new Date();

// console.log(end - start);


// let timer = document.querySelector('.timer');

// function updateTimer(){
//     let now = new Date();
//     let content = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getMilliseconds()}`;
//     timer.textContent = content;
// }


// setInterval(updateTimer, 50);


let mydate = {
    _now: new Date(),

    get now(){
        return `${this._now.getHours()}:${this._now.getMinutes()}:${this._now.getSeconds()}`
    },

    set now(data){
        Object.defineProperty(this, '_now', {
            writable: true
        })
        this._now = data;
        Object.defineProperty(this, '_now', {
            writable: false
        })
    }

}

Object.defineProperty(mydate, '_now', {
    writable: false,
    enumerable: false
})

// Object.defineProperty(mydate, 'time', {
//     get: function(){
//         return `${this.now.getHours()}:${this.now.getMinutes()}:${this.now.getSeconds()}`
//     }
// })


// Object.defineProperty(mydate, 'test', {
//     writable: false,
//     value: 100
// })


mydate.now = 'OK';
mydate._now = 123
console.log(mydate);
