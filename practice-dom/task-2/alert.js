

function toggleAlert(type){
    let greenAlert = document.querySelector('p.alert-green');
    let redAlert = document.querySelector('p.alert-red');

    if(type === 'green'){
        greenAlert.hidden = false;
    }
    else if(type === 'red'){
        redAlert.hidden = false;
    }

}


let userAge = Number(prompt("Enter your age:"));

if (userAge >= 18){
    toggleAlert('green');
}
else {
    toggleAlert('red');
}