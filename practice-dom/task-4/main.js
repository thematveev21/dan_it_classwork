let board = document.querySelector('.board');






function fillChess(){
    let currentColor = 'black';
    for (let row = 0; row < 8; row++) {
        
        for (let column = 0; column < 8; column++) {
            let cell = document.createElement('div');
            cell.classList.add('cell');

            if (currentColor === 'black'){
                cell.style.backgroundColor = '#161619';
                currentColor = 'white';
            }
            else {
                cell.style.backgroundColor = '#ffffff';
                currentColor = 'black';
            }

            board.prepend(cell);
        }

        if (currentColor == 'black'){
            currentColor = 'white';
        }
        else {
            currentColor = 'black';
        }
        
    }
}

fillChess();