const gulp = require("gulp");
const sass = require("gulp-sass")(require('sass'));
const eslint = require("gulp-eslint");
const browserSync = require("browser-sync").create();





function html(){
    return gulp.src("src/index.html")
    .pipe(gulp.dest("build"))
}

function css(){
    return gulp.src("src/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("build"))
}

function js(){
    return gulp.src("src/main.js")
    .pipe(gulp.dest("build"))
}

function watch(){
    browserSync.init({
        server: {
            baseDir: "build"
        }
    })
    gulp.watch("src/**/*.*", gulp.parallel(html, css)).on('change', browserSync.reload)
}




exports.build = gulp.parallel(html, css);
exports.watch = gulp.series(gulp.parallel(html, css), watch);
exports.js = js;