
// // handler function

// function buttonClickHandler(){
//     let title = document.querySelector('h1.title');
//     title.textContent = 'Button clicked!';
//     title.style.backgroundColor = 'red';
// }


// let btn = document.querySelector('.btn');

// // btn.onclick = buttonClickHandler;
// // console.dir(btn);

// btn.addEventListener('click', buttonClickHandler)
// btn.addEventListener('click', buttonClickHandler)
// btn.addEventListener('click', buttonClickHandler)
// btn.addEventListener('click', buttonClickHandler)


/////// example 1

// let box = document.querySelector('.box');

// box.addEventListener(
//     'click',
//     () => {console.log('Left click!')}
// )

// box.addEventListener(
//     'contextmenu',
//     () => {console.log('Right click!')}
// )


// ///// exmaple 2

// let box = document.querySelector('.box');


// function testHandler(event){
//     console.log(event);
// }

// box.addEventListener('click', testHandler)



// ///// ex 2

let box = document.querySelector('.box');

function testHandler(event){
    console.log(event);
    this.hidden = true;
}


box.addEventListener('click', testHandler);