let ball = document.getElementById('ball');
let field = document.getElementById('field');

console.log(ball.offsetHeight)

let minTop = 0;
let maxTop = field.clientHeight - ball.clientHeight;

let minLeft = 0;
let maxLeft = field.clientWidth - ball.clientWidth;

console.log("mintop:", minTop, "maxtop", maxTop, "minleft", minLeft, "maxleft", maxLeft);

function moveBall(left, top){
    console.log("Offset before:", `Left => ${left} | Top => ${top}`)

    if (left > maxLeft) left = maxLeft;
    if (left < minLeft) left = minLeft;
    if (top > maxTop) top = maxTop;
    if (top < minTop) top = minTop;

    console.log("Offset after:", `Left => ${left} | Top => ${top}`)

    ball.style.top = top + 'px';
    ball.style.left = left + 'px';
}


field.addEventListener('click', event => {
    
    let left = event.x - field.offsetLeft - field.clientLeft;
    let top = event.y - field.offsetTop - field.clientTop;

    moveBall(
        left - ball.clientWidth / 2,
        top - ball.clientHeight / 2
    );
})