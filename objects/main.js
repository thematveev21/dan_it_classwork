
// object
// let user = {
//     name: 'Anton',
//     age: 34,
//     salary: 2500
// };

// console.log(user);

// console.log(user);
// console.log(user.name);
// console.log(user.salary)


// ex 1

// let product = {};
// product.name = "Laptop";
// product.price = 1200;
// product.price = 1000;

// console.log(product.name, product.price);

// delete product.price;

// console.log(product.price)


// ex 2

// let a = prompt();
// console.log("User input:", a);

// let info = {
//     name: "Maksim",
//     age: 34,
//     'first-name': "MAKSIM"
// }

// console.log(info['name']);
// console.log(info.name);
// console.log(info['first-name'])
// console.log(info[a]);




// ex 3

// let user = {
//     name: "Alex",
//     age: 34,
//     gender: "M"
// }



// old example
// if (user.gender !== undefined){
//     console.log("Ok");
// }
// else {
//     console.log("Error!")
// }


// new example
// if ('gender' in user){
//     console.log("Ok");
// }
// else {
//     console.log("Error!")
// }



// ex 4

// let user = {
//     name: "Alex",
//     age: 34,
//     gender: "M"
// }

// for (let k in user){
//     console.log(`Key: ${k} Value: ${user[k]}`)
// }


// ex 5

// let salaries = {
//     "Ann": 456,
//     "Viktor": 120,
//     "Peter": 270
// }

// let result = 0;

// for (let k in salaries){
//     result += salaries[k];
// }

// console.log(result);




// ex 6

// let username = "Viktor";
// let age = 34;
// let city = "London";

// // let info = {
// //     username: username,
// //     age: age,
// //     city: city
// // }

// // let info = {
// //     username,
// //     age,
// //     city,
// //     test: true
// // };

// console.log(info);





// ex 7

// let a = {age: 50};

// let b = a;

// b.age = 20;

// console.log(a);
// console.log(b);


// ex 8


// const CONFIG = {
//     ip: "127.0.0.1",
//     port: 5000,
//     login: 'admin',
//     password: 'qwerty'
// };

// CONFIG.ip = "192.168.0.2";

// console.log(CONFIG)


// ex 9

// let first = {
//     num1: 23,
//     num2: 45
// }

// variant 1

// let second = {};

// for (let k in first){
//     second[k] = first[k];
// }

// console.log(first);
// console.log(second);

// console.log(first === second);

// variant 2

// let second = {};
// Object.assign(second, first);

