const hours = document.querySelector('.hours');
const minutes = document.querySelector('.minutes');
const seconds = document.querySelector('.seconds');

console.log(hours);
console.log(minutes);
console.log(seconds);


hours.style.color = 'red';
minutes.style.color = 'green';
seconds.style.color = 'blue';

function beautifyNumbers(number){
    let data = String(number);
    if (data.length < 2){
        return "0" + data;
    }
    else {
        return data;
    }
}



function renderTime(){
    let timeNow = new Date();
    hours.textContent = beautifyNumbers(timeNow.getHours());
    minutes.textContent = beautifyNumbers(timeNow.getMinutes());
    seconds.textContent = beautifyNumbers(timeNow.getSeconds());
}


setInterval(renderTime, 10);