const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const mincss = require("gulp-clean-css");
const shortHand = require("gulp-shorthand");
const autoprefix = require("gulp-autoprefixer");
const browsersync = require("browser-sync").create();


// browser sync

function browserServe(stop){
    browsersync.init({
        server: {
            baseDir: './build'
        }
    })
    stop()
}



// --------


function html(){
    return gulp.src('./src/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build'))
}


function css(){
    return gulp.src('./src/css/**/*.css')
    .pipe(autoprefix({
        cascade: false
    }))
    // .pipe(shortHand())
    .pipe(mincss())
    .pipe(gulp.dest('./build/css'))
}

function watch(){
    gulp.watch('./src/**/*.*', gulp.parallel(html, css)).on('all', browsersync.reload)
}

exports.html = html;
exports.css = css;

exports.dev = gulp.parallel(html, css);
exports.watch = gulp.series(html, css, browserServe, watch);
