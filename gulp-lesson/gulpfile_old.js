const gulp = require("gulp");
const fileInclude = require("gulp-file-include");
const htmlmin = require("gulp-htmlmin");

// task html
// function html(finish){
//     gulp.src("./src/index.html").pipe(gulp.dest("./production"))
//     finish()
// }

function html(){
    return gulp.src("./src/index.html")
    .pipe(fileInclude())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest("./production"))
    
}


exports.html = html