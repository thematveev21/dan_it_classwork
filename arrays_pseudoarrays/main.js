

// let numbers = [12, 23, 34, 45, 56, 67, 78];
// let names = ["Maksim", "Anton", 'Alex', "Viktor"];

// console.log(numbers);
// console.log(numbers[2]);
// console.log(names[0]);

// names[0] = 1000;
// console.log(names);

// console.log(names[99]);

// console.log(names.length)


// ex 2


// let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// console.log(numbers);
// numbers.length = 3;
// console.log(numbers)

// let array = [];
// array[200] = 100;
// console.log(array, array.length);



// ex 3

// let names = ["Anton", "Maksim", "Alina", "Viktor", "Alex"];

// for(let n of names) {
//     console.log(`Your name is ${n}`);
// }

// for (let i = 0; i < names.length; i++){
//     console.log(`${i + 1}. ${names[i]}`)
// }



// ex 4

// let numbers = [1, 2];
// console.log(numbers);


// numbers.push(3) // add to the end
// numbers.push(4)


// console.log(numbers);

// // let first = numbers.shift(); // extract first element
// let last = numbers.pop(); // extaracts last element

// // console.log(first);
// console.log(last);
// console.log(numbers);



// ex 5


// let matrix = [
//     [1, 2, 3],
//     [3, 4, 5],
//     [4, 5, 6],
//     [5, 6, 7]
// ]

// console.log(matrix[2][1])


// ex 6

// let sound = ["Jazz", "Blues"];
// sound.push("Rock-n-Roll");
// console.log(sound);
// sound[1] = "Classics";
// console.log(sound);
// console.log(sound.shift());
// sound.unshift("Rap", "Reggae");
// console.log(sound);



// ex 7

// function sumInput(){
//     let numbers = [];
//     let data;
//     let number;
//     let summa = 0;

//     // do {
//     //     if (number !== undefined){
//     //         numbers.push(number);
//     //     }

//     //     data = prompt("Enter number:");
//     //     number= Number(data);
        
//     // }while(data !== null && !isNaN(number) && data !== "")


//     while (true){
//         let data = prompt("Enter number:");
//         let number = Number(data);

//         if (data == null || isNaN(number) || data == "") {
//             break;
//         }
//         else {
//             numbers.push(number);
//         }

//     }

    
//     for (let a of numbers){
//         summa += a;
//     }


//     return summa;
// }



// ex 8



// let nums = [1, 3, 2, 5, 2, 1]

// let second = [1000, ...nums ,1000]

// console.log(second)


// ex 9


// function studentInfo(name, age, ...marks){
//     console.log(`Name: ${name} Age: ${age} | MAX-MARK: ${Math.max(...marks)}`)
// }

// studentInfo("Alex", 18, 1, 4, 3, 4, 11, 6, 10, 12, 12)