const gulp = require("gulp");
const cssimport = require("gulp-cssimport");
const cssmin = require("gulp-css-minify");
const concat = require("gulp-concat");
const clean = require("gulp-clean");

let dest_folder = "dest"

function html(){
    return gulp.src('./src/index.html')
    .pipe(gulp.dest(dest_folder))
}

function css(){
    return gulp.src('./src/css/main.css')
    .pipe(cssimport())
    .pipe(cssmin())
    .pipe(gulp.dest(dest_folder))
}

function js(){
    return gulp.src('./src/scripts/*.js')
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest(dest_folder))
}

function clearDest(){
    return gulp.src(dest_folder, {allowEmpty: true}).pipe(clean())
}

function watch(){
    gulp.watch("./src/**/*.*", gulp.series(clearDest, gulp.parallel(html, css, js)))
}


exports.run = gulp.series(clearDest, html, css, js)
exports.watch = watch;