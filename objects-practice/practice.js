let rectangle = {_height: 0, _width: 0};

Object.defineProperty(rectangle, 'height', {
    get(){
        return this._height;
    },

    set(height) {
        this._height = Number(height);
        this.p = (this._height + this._width) * 2;
        this.s = this._height * this._width;
    }
});

Object.defineProperty(rectangle, 'width', {
    get(){
        return this._width;
    },

    set(width) {
        this._width = Number(width);
        this.p = (this._height + this._width) * 2;
        this.s = this._height * this._width;
    }
});


rectangle.height = prompt();
rectangle.width = prompt();

console.log(rectangle);