// let form = document.forms.test;
// let input = form.elements.email;
// let p = document.querySelector('p.text');


// input.focus()

// input.addEventListener('blur', () => {
//     let e = input.value;

//     if (!e.includes("@")){
//         input.focus()
//     }
// })


// input.onchange = function(){
//     console.log("Changed!");
// }

// input.oninput = function(){
//     console.log(this.value)
//     if (this.value.includes("@")){
//         p.textContent = "Valid email";
//         p.style.color = 'green';
//     }
//     else {
//         p.textContent = "Invalid email";
//         p.style.color = 'red';
//     }
// }


// form.onsubmit = function(event){
//     event.preventDefault();
//     console.log(event);
// }


// bubling and capture

// let form = document.forms.test;
// let p = document.querySelector(".text");


// document.addEventListener('click', function(event){
//     console.log("Document click!")
//     console.log(event.eventPhase)
// }, true)

// form.addEventListener('click', function(event){
//     console.log("Form click!")
//     console.log(event.eventPhase)
// })

// p.addEventListener('click', function(event){
//     // event.stopPropagation();
//     console.log("P click!")
//     console.log(event.eventPhase)
// })

// form.addEventListener('submit', event => event.preventDefault())



// let form = document.forms.test;

// form.onsubmit = function(){
//     return false;
// }


let buttons = document.querySelector('.buttons');

buttons.addEventListener('click', function(event){
    console.log(event.target.textContent);
})