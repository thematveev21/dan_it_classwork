const timer = document.querySelector('.timer__time');
const startBtn = document.querySelector('.timer__start');
const stopBtn = document.querySelector('.timer__stop');
const resetBtn = document.querySelector('.timer__reset');
const laps = document.querySelector('.timer__laps');
const newLapBtn = document.querySelector('.timer__newlap');
const clearLapsBtn = document.querySelector('.timer__clear')


function renderTimer(hours, minutes, seconds, miliseconds){

    let formatTime = `${hours}:${minutes}:${seconds}.${miliseconds}`;
    timer.textContent = formatTime;
}


function convertMsToTime(milliseconds) {
    let seconds = Math.floor(milliseconds / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);

    milliseconds = milliseconds % 1000;
    seconds = seconds % 60;
    minutes = minutes % 60;
    hours = hours % 24;
    return {hours, minutes, seconds, milliseconds}
  }


function updateTimer(startTime){
    let now = Date.now();
    let gap = now - startTime;
    
    let timeObject = convertMsToTime(gap);
    renderTimer(
        timeObject.hours,
        timeObject.minutes,
        timeObject.seconds,
        timeObject.milliseconds
    );

}

function newLapHandler(){
    let li = document.createElement('li');
    li.textContent = `Lap number: ${lapCounter} | Lap time: ${timer.textContent}`;
    lapsState.push(li.textContent);
    updateLocalStorage();
    laps.append(li);
    lapCounter++;
}


function updateLocalStorage(){
    localStorage.laps = JSON.stringify(lapsState);
}

// main

// global variables

let startTime;
let intervalId;
let lapsState = (localStorage.laps) ? JSON.parse(localStorage.laps) : [];
// let lapsState;
// if (localStorage.laps) {
//     lapsState = JSON.parse(localStorage.laps);
// }
// else {
//     lapsState = [];
// }
let lapCounter = lapsState.length + 1;



lapsState.forEach(text => {
    let li = document.createElement('li');
    li.textContent = text;
    laps.append(li);
});


// handlers
startBtn.addEventListener('click', event => {
    startTime = Date.now();
    intervalId = setInterval(updateTimer, 1, startTime);
})

stopBtn.addEventListener('click', event => {
    clearInterval(intervalId);
})

resetBtn.addEventListener('click', event => {renderTimer(0, 0, 0, 0), startTime = undefined;})
newLapBtn.addEventListener('click', newLapHandler);

clearLapsBtn.addEventListener('click', event => {
    laps.innerHTML = null;
    localStorage.removeItem('laps');
})


